public class Volunteer extends StaffMember{
    Volunteer(int id, String name, String address){
        super(id, name, address);
    }
    public String toString(){
        return  super.toString() + "\nThanks...";
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }
    @Override
    double pay() {
        return 0;
    }
}
