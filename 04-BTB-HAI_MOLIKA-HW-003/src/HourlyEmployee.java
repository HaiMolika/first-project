public class HourlyEmployee extends StaffMember {
    private int hoursWorked;
    private double rate;
    HourlyEmployee(int id, String name, String address, int hoursWorked, double rate){
        super(id, name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }
    public String toString(){
        return super.toString() + "\nHours Worked : " + hoursWorked + "\nRate : " + rate + "\nPayment : " + pay();
    }
    @Override
    double pay() {
        return hoursWorked*rate;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public int getHoursWorked() {
        return hoursWorked;
    }

    public double getRate() {
        return rate;
    }
}
