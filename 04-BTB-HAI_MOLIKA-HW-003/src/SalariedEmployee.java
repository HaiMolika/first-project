public class SalariedEmployee extends StaffMember{
    private double salary, bonus;
    SalariedEmployee(int id, String name, String address, double salary, double bonus){
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }
    public String toString(){
        return super.toString() + "\nSalary : " + salary + "\nBonus : " + bonus + "\nPayment : " + pay();
    }
    @Override
    double pay() {
        return salary + bonus;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public double getBonus() {
        return bonus;
    }

    public double getSalary() {
        return salary;
    }
}
