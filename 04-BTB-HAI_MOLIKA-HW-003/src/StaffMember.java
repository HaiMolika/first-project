public abstract class StaffMember {
    protected int id;
    protected String name, address;

    public StaffMember(int id, String name, String address){
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public int getId() {
        return id;
    }
    
    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public String toString(){
        return "ID : " + id + "\nName : " + name + "\nAddress : " + address;
    }
    abstract double pay();
}
