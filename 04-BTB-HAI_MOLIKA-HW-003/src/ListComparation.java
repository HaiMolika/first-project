import java.util.Comparator;
import java.util.List;

public class ListComparation implements Comparator<StaffMember>{

    @Override
    public int compare(StaffMember o1, StaffMember o2) {
        return o1.name.toUpperCase().compareTo(o2.name.toUpperCase());
    }
}
