import java.util.*;
import java.util.regex.Pattern;

public class MainClass {
    static Scanner in = new Scanner(System.in);
    public static void main(String []args){
        String option;
        ArrayList<StaffMember> list = new ArrayList<>();
        list.add(new Volunteer(1, "Chan Dyka", "#432 Toul Kork"));
        list.add(new HourlyEmployee(2, "Ly Sidyka", "#765 Mean Cheay", 50, 10));
        list.add(new SalariedEmployee(3, "Dy Dary", "#123 Sen Sok", 120, 20));

        while (true){
            Collections.sort(list, new ListComparation());
            for (Object s : list ) {
                System.out.println(s.toString());
                System.out.println("---------------------------------------");
            }
            option = menu();
            switch (option){
                case "1" :
                    addMenu(list);
                    break;
                case "2" :
                    edit(list);
                    break;
                case "3" :
                    remove(list);
                    break;
                case "4" :
                    System.exit(0);
                default:
                    System.out.println("==================================");
                    System.out.println("\tPlease input valid number");
                    System.out.println("==================================");
            }

        }
    }
    static String menu(){
        System.out.println("1). Add Employee\t2). Edit\t3). Remove\t4). Exit");
        System.out.println("==========================");
        System.out.print("=> Choose option(1-4) : ");
        String option = in.nextLine();
        System.out.println("==========================");
        return option;
    }
    static void addMenu(ArrayList<StaffMember> list){
        int id, hour;
        String name, address;
        boolean isExist = false;
        double rate, salary, bonus;
        System.out.println("1). Volunteer\t2). Hourly Emp\t3). Salaried Emp\t4). Back");
        System.out.println("==========================");
        System.out.print("=> Choose option(1-4) : ");
        String option = in.nextLine();
        System.out.println("==========================");
        switch (option){
            case "1": //Volunteer
                System.out.println("\t=============== INSERT INFO ===============");
                System.out.print("=> Enter Staff Member's ID : ");
                id = in.nextInt();
                System.out.print("=> Enter Staff Member's Name : ");
                in.nextLine();
                name = in.nextLine();
                System.out.print("=> Enter Staff Member's Address : ");
                address = in.nextLine();
                for (StaffMember s : list) {
                    if(id == s.getId()) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    list.add(new Volunteer(id, name, address));
                    System.out.println("==================================");
                    System.out.println("\tSuccessfully added.");
                    System.out.println("==================================");
                    isExist = false;
                }
                else {
                    System.out.println("==================================");
                    System.out.println("\tID is already EXIST.");
                    System.out.println("==================================");
                }

                break;
            case "2" : //Hourly Emp
                System.out.println("\t=============== INSERT INFO ===============");
                System.out.print("=> Enter Staff Member's ID : ");
                id = in.nextInt();
                System.out.print("=> Enter Staff Member's Name : ");
                in.nextLine();
                name = in.nextLine();
                System.out.print("=> Enter Staff Member's Address : ");
                address = in.nextLine();
                System.out.print("=> Enter Hours Worked : ");
                hour = in.nextInt();
                System.out.print("=> Enter Rate : ");
                rate = in.nextDouble();
                in.nextLine();
                for (StaffMember s : list) {
                    if(id == s.getId()) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    list.add(new HourlyEmployee(id, name, address, hour, rate));
                    System.out.println("==================================");
                    System.out.println("\tSuccessfully added.");
                    System.out.println("==================================");
                    isExist = false;
                }
                else {
                    System.out.println("==================================");
                    System.out.println("\tID is already EXIST.");
                    System.out.println("==================================");
                }
                break;
            case "3" : //Salaried Emp
                System.out.println("\t=============== INSERT INFO ===============");
                System.out.print("=> Enter Staff Member's ID : ");
                id = in.nextInt();
                System.out.print("=> Enter Staff Member's Name : ");
                in.nextLine();
                name = in.nextLine();
                System.out.print("=> Enter Staff Member's Address : ");
                address = in.nextLine();
                System.out.print("=> Enter Staff's Salary : ");
                salary = in.nextDouble();
                System.out.print("=> Enter Staff's Bonus : ");
                bonus = in.nextDouble();
                for (StaffMember s : list) {
                    if(id == s.getId()) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    list.add(new SalariedEmployee(id, name, address, salary, bonus));
                    System.out.println("==================================");
                    System.out.println("\tSuccessfully added.");
                    System.out.println("==================================");
                    isExist = false;
                }
                else {
                    System.out.println("==================================");
                    System.out.println("\tID is already EXIST.");
                    System.out.println("==================================");
                }
                in.nextLine();
                break;
            case "4" :
                break;
            default:
                System.out.println("==================================");
                System.out.println("\tPlease input valid number");
                System.out.println("==================================");

        }
    }
    static void edit(ArrayList<StaffMember> list){
        int nid=0,id=0, hour=0, index=-1;
        String name = "", address = "", post ="";
        double rate=0, salary=0, bonus=0;
        boolean isExist = false;
        System.out.println("\t=============== EDIT INFO ===============");
        System.out.print("=> Enter Employee ID to update : ");
        nid = in.nextInt();
        for (StaffMember s : list) {
            if(nid == s.getId()) {
                System.out.println();
                System.out.println(s.toString());
                post = s.getClass().getName();
                index = list.indexOf(s);
                isExist = true;
                break;
            }
        }
        if (isExist){
            StaffMember obj = null;
            isExist = false;
            System.out.println("\t=============== NEW INFORMATION OF STAFF MEMBER ===============");
            if (post.equals("Volunteer")){
                System.out.print("=> Enter Staff Member's ID : ");
                id = in.nextInt();
                System.out.print("=> Enter Staff Member's Name : ");
                in.nextLine();
                name = in.nextLine();
                System.out.print("=> Enter Staff Member's Address : ");
                address = in.nextLine();
                obj = new Volunteer(id, name, address);
            }
            else if(post.equals("HourlyEmployee"))
            {
                System.out.print("=> Enter Staff Member's ID : ");
                id = in.nextInt();
                System.out.print("=> Enter Staff Member's Name : ");
                in.nextLine();
                name = in.nextLine();
                System.out.print("=> Enter Staff Member's Address : ");
                address = in.nextLine();
                System.out.print("=> Enter Hours Worked : ");
                hour = in.nextInt();
                System.out.print("=> Enter Rate : ");
                rate = in.nextDouble();
                in.nextLine();
                obj = new HourlyEmployee(id, name, address, hour, rate);
            }
            else if (post.equals("SalariedEmployee"))
            {
                System.out.print("=> Enter Staff Member's ID : ");
                id = in.nextInt();
                System.out.print("=> Enter Staff Member's Name : ");
                in.nextLine();
                name = in.nextLine();
                System.out.print("=> Enter Staff Member's Address : ");
                address = in.nextLine();
                System.out.print("=> Enter Staff's Salary : ");
                salary = in.nextDouble();
                System.out.print("=> Enter Staff's Bonus : ");
                bonus = in.nextDouble();
                in.nextLine();
                obj = new SalariedEmployee(id, name, address, salary, bonus);
            }
            for (StaffMember s : list) {
                if(id == s.getId() && id != nid) {
                    System.out.println();
                    System.out.println(s.toString());
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                list.set(index, obj);
                System.out.println("==================================");
                System.out.println("\tSuccessfully Edit.");
                System.out.println("==================================");
                isExist = false;
            }
            else {
                System.out.println("==================================");
                System.out.println("\tID is already EXIST.");
                System.out.println("==================================");
            }
        }
        else {
            System.out.println("==================================");
            System.out.println("\tID is not already EXIST.");
            System.out.println("==================================");
        }
    }
    static void remove(ArrayList<StaffMember> list){
        int nid=0;
        boolean isExist = false;
        System.out.println("\t=============== EDIT INFO ===============");
        System.out.print("=> Enter Employee ID to delete : ");
        nid = in.nextInt();
        for (StaffMember s : list) {
            if(nid == s.getId()) {
                System.out.println();
                System.out.println(s.toString());
                System.out.println();
                list.remove(s);
                System.out.println("==================================");
                System.out.println("\tSuccessfully Delete...");
                System.out.println("==================================");
                isExist = true;
                break;
            }
        }
        if (!isExist){
            System.out.println("==================================");
            System.out.println("\tID is not already EXIST.");
            System.out.println("==================================");
        }
        in.nextLine();
    }

}
